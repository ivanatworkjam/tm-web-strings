msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"Language: en\n"


msgid "all_actionDelete"
msgstr "Delete"

msgid "all_actionEdit"
msgstr "Edit"

#,src/modules/task-management/ui/Categories/Categories.tsx
msgid "taskManagement_category_description"
msgstr "Create categories and assign them to tasks to easily track and drill down for reporting. Organize tasks into a category tree by adding subcategories to a parent task."

#,src/modules/task-management/ui/Categories/Categories.tsx
msgid "taskManagement_category_searchLabel"
msgstr "Search for category"

#,src/modules/task-management/ui/Categories/Header.tsx
msgid "taskManagement_category_actionCreate"
msgstr "Create category"

#,src/modules/task-management/ui/Categories/Header.tsx
msgid "taskManagement_category_headerCategoryTitle"
msgstr "Task Categories"

#,src/modules/task-management/ui/Categories/CategoryRow.tsx
msgid "taskManagement_category_actionCreateSubCategory"
msgstr "Create subcategory"
